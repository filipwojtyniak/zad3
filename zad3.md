---
author: Filip Wojtyniak
title: Prezentacja o Queen
date: 02.11.2022
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---



## Powstanie zespołu

W 1968 Brian May, wówczas student Imperial College w Londynie, skompletował skład zespołu Smile poprzez uczelnianą tablicę ogłoszeń. Głównym wokalistą i basistą został Tim Staffell, zaś perkusistą – Roger Taylor. Grupa grała głównie w pubach i college’ach.

Smile podpisał jednorazową, niekorzystną umowę z wytwórnią Mercury Records. Efektem było wydanie singla Earth, który jednak ukazał się tylko w Stanach Zjednoczonych (gdzie zespół był praktycznie nieznany), a jego premiery nie poprzedziła żadna promocja. Po komercyjnej porażce singla Staffell odszedł do zespołu Humpy Bong, natomiast May i Taylor wkrótce potem rozpoczęli współpracę ze współlokatorem Staffella – wokalistą, który później stał się znany pod nazwiskiem Freddie Mercury.

Kilka utworów Smile zostało w późniejszym okresie nagranych ponownie przez Queen, w tym „Doin’ All Right”, umieszczony na albumie Queen.

Zespół został założony w 1970 roku, gdy do Rogera Taylora i Briana Maya, wówczas członków rozpadającej się formacji Smile, dołączył Freddie Mercury. Czwartym stałym członkiem zespołu został w 1971 roku John Deacon. Zgodnie z pomysłem Mercury’ego, zespół przyjął nazwę Queen.

![Queen1](E:\zad3\Queen1.png)



## Pierwsza piosenka

### Pierwszy nagrany singiel zespołu to „Stone Cold Crazy”

Pierwszy występ Queen odbył się w czerwcu 1971 roku, pierwszym wykonanym utworem, zgodnie ze słowami Mercury’ego był „Stone Cold Crazy”, zaś prace nad pierwszymi nagraniami studyjnymi rozpoczęły się w 1972, gdy zespół otrzymał propozycję przetestowania wyposażenia nowego studia De Lane Lea. Powstałe wtedy wersje demo utworów nie zainteresowały żadnej wytwórni, ale pracownicy studia – Roy Thomas Baker i John Anthony – namówili swoich szefów do współpracy z zespołem.






## Najbardziej popularne utwory The Beatles




- ***„We Will Rock You”***
- ***„Bohemian Rhapsody”***
- ***„We Are The Champions”***
- ***„Radio Ga Ga”***

## Członkowie

### John Deacon

![Queen3](E:\zad3\Queen3.jpg)



**John Richard Deacon** (ur. 1951) jest najmłodszym członkiem zespołu i wstąpił do niego najpóźniej (1971). Z wykształcenia jest elektronikiem. Przed Deaconem zespół próbował współpracy z kilkoma basistami – Mike Grose (3 koncerty), Barry Mitchell (9 koncertów), Doug Bogie (2 koncerty). Pierwszy występ z Deaconem miał miejsce 2 lipca 1971. Brian May wspominał później:
*Czuliśmy, że jest on tym właściwym, chociaż był taki spokojny. Prawie wcale się do nas nie odzywał!.*



### Brian May

![Queen4](E:\zad3\Queen4.jpg)



**Brian Harold May** (ur. 1947) rozpoczął karierę muzyczną w wieku 15 lat, grając w lokalnych zespołach. Rok później rozpoczął wraz z ojcem budowę swojej własnej gitary, Red Special. W 1967 rozpoczął studia na wydziale fizyki w Imperial College w Londynie. Na uczelni poznał Tima Staffella i Rogera Taylora, z którymi założył zespół Smile. Grupa rozpadła się po wydaniu jednego singla, ale Staffell przed swoim odejściem poznał Maya i Taylora z Mercurym i ta trójka wkrótce potem założyła Queen.



### Roger Taylor

![Queen5](E:\zad3\Queen5.jpg)



**Roger Meddows-Taylor** (ur. 1949) początkowo zamierzał zostać gitarzystą, dopiero później zainteresował się perkusją. Studiował stomatologię, później przeniósł się na wydział biologiczny Imperial College. Odpowiedział na ogłoszenie Maya (poszukującego chętnych do współpracy w zespole) tuż po Staffellu.



### Freddie Mercury

![Queen6](E:\zad3\Queen6.jpg)

**Farrokh (Frederick) Bulsara** (1946–1991) był absolwentem studiów plastycznych w Ealing College of Art. Jednymi z jego pierwszych zespołów były: Sour Milk Sea i Wreckage. Później wraz z Mayem i Taylorem stworzył zespół Queen, a wkrótce potem zmienił nazwisko na Freddie Mercury. To on był pomysłodawcą nazwy, zaprojektował logo i wyznaczył kierunek artystyczny, w którym zespół miał się rozwijać.

## Przełom (1974–1979)



### Killer Queen

Punktem zwrotnym kariery Queen był singel „Killer Queen” (napisany, jak twierdził Mercury, w jedną noc[15]), promujący następny album, Sheer Heart Attack. Od tej pory popularność zespołu rosła gwałtownie, zwłaszcza w Japonii, gdzie nowy album trafił na pierwsze miejsce w zestawieniach. Sheer Heart Attack stanowił przełom w muzycznym stylu Queen – wcześniejsze albumy miały wyraźne cechy rocka progresywnego, tutaj mamy do czynienia z muzyką lżejszą i łatwiejszą w odbiorze[16]. Trend ten stał się jeszcze wyraźniejszy w następnym albumie. Jeden z koncertów Queen w Rainbow Theatre w Londynie został zarejestrowany na kasecie wideo, wydanej później pod tytułem Live at the Rainbow.

![Queen2](E:\zad3\Queen2.jpg)


## Koniec Legendy

23 listopada 1991 Freddie Mercury podał do publicznej wiadomości, za pośrednictwem Press Association, oświadczenie o swojej chorobie. Nieco ponad 24 godziny później zmarł w swoim domu w Kensington w Londynie.

Krótko po śmierci Mercury’ego został wydany specjalny singiel – „Bohemian Rhapsody” / „These Are the Days of Our Lives”. Przychód ze sprzedaży płyty (około 1 miliona funtów) został przeznaczony na walkę z AIDS.

12 lutego 1992 May i Taylor w imieniu zespołu odebrali kolejną nagrodę Brit Awards, tym razem za singiel roku („These Are the Days of Our Lives”). 20 kwietnia na stadionie Wembley w Londynie odbył się koncert poświęcony pamięci Freddiego Mercury’ego. Oprócz pozostałych członków Queen, wystąpili na nim: Def Leppard, Lisa Stansfield, Elton John, David Bowie, Robert Plant, Tony Iommi, Annie Lennox, Guns N’ Roses, Extreme, Roger Daltrey, George Michael, Ian Hunter, Mick Ronson, Zucchero, Metallica, Liza Minnelli, Elizabeth Taylor, Spinal Tap.
